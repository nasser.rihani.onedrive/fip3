import 'package:fip3/anas/list/listview_builder.dart';
import 'package:fip3/anas/navigation/args/register_Screen.dart';
import 'package:flutter/material.dart';
import 'anas/image_picker/image_picker_screen.dart';
import 'anas/list/await_example.dart';
import 'anas/list/future_builder_example.dart';
import 'anas/list/list_view_builder_2.dart';
import 'anas/list/stream_builder_example.dart';
import 'anas/mix_widgets3.dart';
import 'anas/navigation/args/login_screen.dart';
import 'anas/navigation/screen1.dart';
import 'anas/navigation/screen2.dart';
import 'anas/navigation/screen3.dart';
import 'anas/text_form_field.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key); // constructor

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
          primarySwatch: Colors.red,
          accentColor: Colors.green,
          backgroundColor: Colors.black,
          // scaffoldBackgroundColor: Colors.green,
          buttonColor: Colors.green,
          cardColor: Colors.white30),
      darkTheme: ThemeData(
          primarySwatch: Colors.red,
          accentColor: Colors.green,
          backgroundColor: Colors.black,
          // scaffoldBackgroundColor: Colors.green,
          buttonColor: Colors.redAccent,
          cardColor: Colors.white30),
      debugShowCheckedModeBanner: false,
      routes: {
        "/screen1":(context)=>Screen1(),
        "/screen2":(context)=>Screen2(),
        "/screen3":(context)=>Screen3(),
        "/login":(context)=>LoginScreen(),
        "/register":(context)=>RegisterScreen(),
        "/mix3":(context)=>MixWidgets3(),
        "/TextFormField2":(context)=>TextFormField2(),
        "/listviewbuilder":(context)=>ListViewBuilder2(),
        "/awaitExample":(context)=>AwaitExample(),
        "/FutureBuilderUi":(context)=>FutureBuilderUi(),
        "/StreamBuilderUi":(context)=>StreamBuilderUi(),
        "/ImagePickerScreen":(context)=>ImagePickerScreen(),
      },
      initialRoute: '/ImagePickerScreen',
      // home: SafeArea(child: const Screen1()),
    );
  }
}

class Home extends StatelessWidget {
  const Home({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('FIP3'),
        centerTitle: true,
        backgroundColor: Colors.green,
        actions: [
          const Icon(Icons.build),
          const Icon(Icons.add),
          const Icon(Icons.add),
          const Icon(Icons.add),
        ],
        leading: const Icon(Icons.map),
        elevation: 20,
      ),
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Align(child: Text('Sami'), alignment: Alignment.centerRight),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              // crossAxisAlignment: CrossAxisAlignment.center,
              children: const [
                Text('hello'),
                Icon(Icons.store),
                Text('Carfour'),
                Text('Carfour'),
                Text('Carfour'),
                Text('Carfour'),
                Text('Carfour'),
                Text('Carfour'),
                Text('Carfour'),
                Text('Carfour'),
                Text('Carfour'),
              ],
            ),
          ),
          Text('hello'),
        ],
      )),
    );
  }
}
