import 'package:flutter/material.dart';

void main() {
  runApp(const Main());
}

class Main extends StatelessWidget {
  const Main({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: NasserRihani(),
    );
  }
}

class NasserRihani extends StatefulWidget {
  const NasserRihani({Key key}) : super(key: key);

  @override
  State<NasserRihani> createState() => _NasserRihaniState();
}

class _NasserRihaniState extends State<NasserRihani> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text("Nasser"),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Icon(Icons.add),
                Icon(Icons.add),
                Icon(Icons.add),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Sami"),
                Text("Maher"),
              ],
            ),
            Align(child: Text("Rana"),alignment: Alignment.centerRight,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Icon(Icons.ac_unit),
                Icon(Icons.ac_unit),
              ],
            ),
            Align(child: Text("Aseel"),alignment: Alignment.centerLeft,)
          ],
        ),
      ),
    );
  }
}
