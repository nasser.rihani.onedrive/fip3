
class Home {

  List<Room> rooms;
  List<Door> doors;

  Home(this.rooms, this.doors);

  @override
  String toString() {
    return 'Home{rooms: $rooms, doors: $doors}';
  }
}
class Room {
  List<Window> windows;
  List<Bed> beds;
  double area;
  bool isMaster;
  Room(this.windows, this.beds, this.area,this.isMaster);

  @override
  String toString() {
    return 'Room{windows: $windows, beds: $beds, area: $area, isMaster: $isMaster}';
  }
}
class Door{
  double height;
  double thickness;
  String doorType;

  Door(this.height, this.thickness, this.doorType);

  @override
  String toString() {
    return 'Door{height: $height, thikness: $thickness, doorType: $doorType}';
  }
}
class Window{
  String windowType;
  double width;
  double thickness;

  Window(this.windowType, this.width, this.thickness);

  @override
  String toString() {
    return 'Window{windowType: $windowType, width: $width, thickness: $thickness}';
  }
}
class Bed {
  double height;
  double width;
  Bed(this.height, this.width);

  @override
  String toString() {
    return 'Bed{height: $height, width: $width}';
  }
}
void main(){
  Bed bed1=Bed(200, 100);
  Bed bed2=Bed(200, 100);
  Bed bed3=Bed(200, 100);
  Window window1=Window('Glass', 100, 4);
  Window window2=Window('Glass', 110, 4);
  Window window3=Window('wood', 120, 8);

  Door door1=Door(200, 35, 'Wood');
  Door door2=Door(200, 35, 'metal');
  Door door3=Door(210, 40, 'metal');

  Room room1=Room([Window('Wood', 300, 25),window2], [bed1,bed3], 18,true);
  Room room2=Room([window1,window3], [bed1,bed2], 15,false);
  Room room3=Room([window1,window1], [bed1,bed1], 16,false);
  Home home=Home([room1,room2,room3],[door1,door2,door3]);
  // Color redColor=Colors.red;
  print(home.toString());
  CarType type=CarType.kia;
  // compisition has a
}
// Car class
/*
carType Type
Date model
String name
List<Wheel> wheels
Motor motor
Color bodyColor
* */
/*
wheel class
thickness
long
* */
/*
motor class
type
cc
hourses
* */
/*
Color
* */
enum CarType {
  hyundai,kia,mercedes
}