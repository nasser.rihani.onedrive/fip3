import 'inheritance.dart';

class Test<T> {
  T data;

  Test(this.data);

  String msg() {
    return 'Hello' + data.toString();
  }
  T getData(){
    return data;
  }
}

class Test22 {
  Person data;

  Test22(this.data);

  String msg() {
    return 'Hello' + data.toString();
  }
  Person getData(){
    return data;
  }
}

void main() {
  Student ali = Student(
      // alt + enter to import
      name: 'Ali',
      age: 35,
      idNumber: '9921445623',
      dateOfBirth: '05/11/1997',
      ssn: '9981014331',
      major: 'Cs',
      mark: '88',
      studentNumber: '516165156165');
  Test<Person> test = Test(ali);
  print(test.msg());
  Test<Person> test2 = Test(Person(
      name: 'Anas',
      dateOfBirth: '1561651',
      idNumber: '1561561',
      ssn: '1615646',
      age: 88));
  test2.msg();
  Test<int> test3=Test(5);
  Test<String> test4=Test('5');
  print(test3.msg());
  print(test.getData());
}
