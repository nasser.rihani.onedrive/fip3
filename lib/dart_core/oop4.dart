class Test {
 static int counter=0; // shared

}

class Test2{

  void increment(){
    Test.counter++;
  }

}
void main(){
  // Test test=Test(); // object address
  // test.counter++; // 1
  // Test test2=Test(); // object diff address
  // test2.counter++; // 1
  // test2.counter++; // 2
  // print(test.counter); // 1
  // print(test2.counter); // 2

  Test2  test=Test2();
  Test2  test2=Test2();
  test.increment();
  test2.increment();
  print(Test.counter); //

}
