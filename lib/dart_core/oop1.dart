import 'employee_class.dart';
class Hello {
  String name;
  int age;
}
class Person { //
  // constructor
  // default values
  // upper camel case convention
  String name;
  double age;
  String ssn;

    Person({this.name, this.age});

  void printData() {
    print('the name of the person is $name');
    print('the age of the person is $age');
    print('the social security number of the person is $ssn');
  }
}

void main() {
  Employee hala=new Employee('Hala', 22, 1800, '', 2); // alt + enter to import
  print(hala.getUniCert('')); // method
  print(hala.uniCert); // Get
  hala.setUniCert("Cis"); // methos
  hala.uniCert="SE"; // Set

  print('hello');
  // new object ctrl space
  Person ahmad = Person(name: "Ahmad",age: 20); // new object // @1
  // Person anas= Person(); // new object  // @2
  // Person sami= Person(); // new object  // @3
  ahmad.ssn = "20220156165";

  print(ahmad.name);
  print(ahmad.age);
  print(ahmad.ssn);
  ahmad.printData();
}
