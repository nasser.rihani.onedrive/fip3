import 'dart:math' as math;

import 'package:fip3/dart_core/employee_class.dart';

void main() {
  print(5);
  print("hello");
  print('hello');
  print('hello');
  print('hello');
  print('hello');
  print('hello');
  print('hello');
  print('hello');
  print('hello');

  print('abdallah');
  print('abdallah');
  print('abdallah');
  print('abdallah');
  print('abdallah');
  print('abdallah');
  print('abdallah2');
  print('abdallah2');
  print('anas');
  print('anas');
  // variables

  // data types

  // int  20 , 2000 , -500
  // double  15.3 , -22.65
  // String "hello world "  , 'hello yazan '
  // bool true , false
  // num 20 , -233.334
  // dynamic "anas" , 445 , 98.34
  // var

  // syntax

  // data type   // variable name  ;
  // data type    // name   =  value
  int number; // decleration
  number = 10; // initalization
  number = 30;
  // number="anas"; // syntax error
  number = 22;

  int number2 = 20; // decleration + initalization
  print(number);
  print(number2);
  int number3 = number + number2; // sum add
  print(number3);

  String name = "Ahmad";
  print(name); // Ahmad
  name = "Sami";
  print(name); // Sami
  String word = "Talal";
  word = word + ' Ahmad'; // Talal Ahmad  // concatenation

  print(word);

  String s = "hello"; // new object
  //int // 4 byte
  //double // 8 byte
  // num 16 byte
  s = "World";
  ///////
  s = s + 'World';
  print(s);

  bool isMale = true;
  bool isFeMale = false;

  double realNumber = 20;
  print(realNumber); // 20.0
  realNumber = 22.5;
  print(realNumber);

  num x = 20; // 16 byte
  print(x);
  x = 22.2; // 4 byte
  print(x);

  dynamic w = "anas";
  w = 8;
  w = false;
  var e = 5;
  e = 55;
  e = 22;

  String char = '#';
  String c = '';

  // *********************************//

  int y = -21;
  print(y.isNegative); // true
  print(y.isEven); // false
  print(y.abs()); // 21
  y = y.abs(); // 21
  print('*****************************');
  double p = -22.5; // 22.5    23     22
  print(p.ceil());
  print(p.floor());
  print(p.round());
  print(p.toInt()); // convert from type to another type (casting)
  print(y.toDouble()); // casting
  print(p.toStringAsFixed(2));
  print(p.toStringAsExponential(3));

  // ***************************************//
  print('****************************');
  String anas = "Anas pp"; //
  print(anas);
  print(anas.isEmpty);
  print(anas.length);
  print(anas.isNotEmpty);
  print(anas.contains("pp"));
  print(anas.contains("pP")); // case sensetive
  print(anas == 'Anas Pp');
  print(anas.toLowerCase());
  print(anas.toUpperCase());
  anas = anas.toLowerCase(); // assign
  print(anas.toUpperCase() == 'AnAS Pp'.toUpperCase()); // equalsIgnoreCase
  print(anas);
  print(anas.indexOf('A'));
  print(anas.indexOf('p')); // 5
  print(anas.indexOf('pp')); // 5
  print(anas.lastIndexOf('p')); // 6
  print(anas.startsWith('An'));
  print(anas.startsWith('an'));
  print(anas.endsWith('pp'));
  print(anas.endsWith('Anas pp'));

  // ctrl + /    to comment code
  // ctrl + alt + l  reformat

  String ahmad = "ahmad";
  ahmad = ahmad + ' quraan';
  print(ahmad.lastIndexOf('a'));
  String mohammad = "mohammad";
  print(mohammad.length);
  String hala = "hala";
  print(hala.contains("o"));
  print(hala.indexOf('o'));
  print(hala.indexOf('la'));
  double A = 8.1;
  int a = A.round();
  print(a);

  print('*******************************************');
  // naming convension
  // lower camel case
  // firstName x
  // secondName
  // passwordLength
  // Password
  // passwordlength
  /*

    A variable name can only have letters (both uppercase and lowercase letters), digits and underscore.
    The first letter of a variable should be either a letter or an underscore.
    a variable should not be same as keywords  or data types(new const final int String bool ...)    https://www.geeksforgeeks.org/dart-keywords/


  *  */
  //

  String fName = '';
  String ANAS2 = 'Anas';

  // print(ANAS);
  // String new=''; // syntax error
  print('*************************************');
  print("hello".codeUnits);
  print("hello".replaceAll("h", "P"));
  print("hello".replaceAll('ll', 's'));
  print('hello'.replaceFirst('l', 'L'));
  print('hello'
      .replaceRange(0, 3, '0')); // (0,3] 0 , 1 2 and 3 not included 000lo  0lo
  print('hellohello'.replaceAll('ll', 'p')); // hepphepo
  print(
      'hello'.substring(1, 3)); // use nested strings el  3 not included 0 1  el
  print('hello'.substring(3)); // lo
  print('hello'.substring(4)); // o
  print('hello'.substring(
      5)); //   h(0) e(1) l(2) l(3) o(4)  5   runtime exception  flutter 2 null safe  empty string
  String international = 'international';
  print(international.substring(
      2, international.indexOf('i', 2) + 1)); // start index for searching
  // i(0) n(1) t(2) e(3) r(4) n(5) a(6) t(7) i(8) o(9) nal  // ternati
  // hello index pointer
  print('    hello  '.trim()); // remove first and last spaces
  print('    hel      lo  '.trim()); // remove first and last spaces
  print('    hello  '.trimRight()); // remove right  spaces   '     hello'
  print('    hello  '.trimLeft()); // remove left spaces   'hello     '

  // priority     () ^ (xor)  math.pow      */  +-   left to right
  print('********************* numbers operators *********************');
  print(5 * 3);
  print(5 * 3 + 5);
  print(5 + 3 * 4);
  print((5 + 19) * 8 + (1 + 15)); // 24 *8 + 16 = 192+16=208
  print((5 + (3 * 8 / 2) - 10 * (15 - 8))
      .toInt()); // 5+ 12 - 10 * 7   5 + 12 - 70 , 17 - 70 = -53
  // power
  print(math.pow(5, 2));
  print(math.pow(5, 2) * 3);
  print(10 % 3); // module 10/3= 3 ~ 1
  print(10 % 2); // 0
  print((5 + 2 * (33 - 22 / 11)) * 2 - 20 * (55 - 35)); // 299 -299  -266.0

  print(5 > 3); //true
  print(11 < 11); // false
  print(5 >= 5); // true
  print(5 <= 5); // true
  print(5 > 3 || 5 > 8); // true  T or T =true
  print(5 > 3 && 5 > 8); // false  T and F =false
  print(5 > 8 && 5 > 3); //  false
  double max = 5;
  print(max);
  max++; // max = max +1 ;
  max--; // max=max-1;
  max *= 100; // max=max*2;
  max /= 2; // max=max/2;
  double min = 8;
  print(min--); // 8
  print(min); // 7
  print(--min); //6
  min = 10;
  max = 20;
  print(5 * (--min) +
      max++ +
      20 +
      max); // min = 9 max = 21  result  5 * 9 + 20 +20+21 =
  min /= 2; // 4.5
  max *= 3; // 63

  // postfix prefix

  /*
   or table (at least one true to be always true )
   true or true = true
   true or false = true
   false or true = true
   false or false = false


   and table (at lease one false to be always false )

   true and true = true
   false and true = false
   true and false = false
   false and false = false

  */

  print('********************* end numbers operators *********************');

  // if statement
  // syntax
  // if (confition) { }  optional else { }
  int age = 10;
  if (age >= 18) {
    print('success');
    print('success');
  } else {
    if (age > 15)
      print('soon');
    else {
      if (age > 13) print('failed');
    }
  } // nested if
  if (age >= 18) {
    print('success');
    print('success');
  } else if (age > 15) {
    print('soon');
  } else if (age > 13) {
    print('failed');
  }
  age = 30;
  if (age > 18) print('age>18');
  if (age > 20) print('age>20');
  if (age > 25) print('age>25');

  if (age >= 18) {
    print('age>18');
  } else if (age > 20) {
    print('age>20');
  } else if (age > 25) {
    print('age>25');
  }

  /*  syntax   switch (variable to be compare)
  case ---- : execution ; break


   */
  age = 20;
  switch (age) {
    case 18:
      print('age is 18');
      break;
    case 19:
      print('age is 19');
      break;
    case 20:
      print('age is 20');
      break;
  }

  // write a code to compare between three variables num1 , num2 , num3  and print the biggest number
  // num1 = 40   num2 = 55 , num3=-20

  int num1 = 10;
  int num2 = 20;
  int num3 = 30;

  int minm = num1;

  if (num2 < minm) minm = num2;
  if (num3 < minm) minm = num3;

  print(minm);

  // loop

  // for (init val (optional);  condition (mandatory) ; inc/dec (optional))
  //{

  //}

  for (int index = 0; index < 10; index++) {
    print(index); // 0 1 2 3 4 5 6 7 8 9
  }

  int index = 0;
  for (; index < 10; index++) {
    print(index);
  }
  index = 0;
  // for (; index < 10;) {
  //   print(index); // infinite loop
  // }
  for (int counter = 10; counter >= 1; counter--) {
    print(counter); // 10 9 8 7 6 5 4 3 2 1
  }

  for (int counter = 0; counter < 5; counter++) {
    if (counter == 3) continue; // keyword skip jump
    print(counter); // 0 1 2 4
  }
  for (int counter = 0; counter < 5; counter++) {
    if (counter == 3) break; // keyword end loop
    print(counter); // 0 1 2
  }

  // while
  /*
  while(condition) {

  }

    */
  age = 11;
  while (age > 10) {
    print(age); //11
    age--;
  }

  while (age > 12) {
    print('age > 12');
  }
  // while (age > 8) {
  //   if (age > 10) continue; // stack over flow exception
  //   print(age); // 10
  //   age++;
  // } //

  for (int counter = 2; counter <= 100; counter++) {
    if (counter.isEven) print(counter);
  }
  for (int counter = 1; counter <= 100; counter++) {
    if (counter % 2 == 1) print(counter);
  }

  print('*********************************************');
  // List
  List<int> data = [];
  data.add(5);
  data.add(22);
  data.addAll([15, 17, 55, 62, 88, 77, 22]);
  data.replaceRange(0, 2, [
    5,
    5,
  ]);
  print(data.any((element) => element > 5));
  print(data.indexWhere((element) => element == 77));
  print(data.remove(5));
  print(data.removeAt(5));
  data.sort();
  print(data);
  data.sort(
    (a, b) => b,
  );
  print(data);
  print(data.last);
  print(data.first);
  data.insert(0, 2);
  print(data);
  data.insertAll(0, [1, 2, 3]);
  print(data);
  var newList = data.where((element) => element > 20);
  print(newList);
  Map map = {"vgg": "ggg", "dfd": 5, "tyr": -20};
  map.update("dfd", (value) => 2);
  print(map);

  print('************************************');



}
