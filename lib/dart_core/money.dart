class Money {
  // upper camel case

  double _value;
  String _currencyCode;
  double _inUsd;

  double get value => _value;

  set value(double value) {
    _value = value;
  }

  Money(this._value, this._currencyCode, this._inUsd);

  double convertToUsd() {
    if(this._inUsd>=0.71) {
      return double.tryParse((_value / _inUsd).toStringAsFixed(1));
    }
    return double.tryParse((_value * _inUsd).toStringAsFixed(1)); // 5 / 0.71 = 7.01
// 5 / 0.71 = 7.01
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
      other is Money &&
          convertToUsd() == other.convertToUsd();
  }

  @override
  int get hashCode =>
      _value.hashCode ^ _currencyCode.hashCode ^ _inUsd.hashCode;


  // @override
  // String toString() {
  //   return 'Money{_value: $_value, _currencyCode: $_currencyCode, _inUsd: $_inUsd}';
  // }

  String get currencyCode => _currencyCode;

  double get inUsd => _inUsd;



  void subtract(double val){
    if(val>_value)
      {
        print('can not subtract more that the original value');
        return;
      }
    if(double.tryParse(_value.toStringAsFixed(2))<=0.05) {
      print('can not subtract');
      return;
    }
    _value-=val;
  }
  void add (double val){
    if(val.isNegative) {
      print('can not add negative value');
      return;
    }
    _value+=val;
  }

  @override
  String toString() {
    return '_value: $_value, _currencyCode: $_currencyCode, _inUsd: $_inUsd ';
  }
}

void main() {
  Money jod5 = Money(5, 'JOD', 0.71);
  print(jod5.convertToUsd().toStringAsFixed(2) + ' in usd');
  Money jod10 = Money(10, 'JOD', 0.71); // @a10
  print(jod10.convertToUsd().toStringAsFixed(2) + ' in usd');
  Money jodd10 = Money(10, 'JOD', 0.71); // @a11
  print(jod10==jodd10); // true false
  jod10.subtract(-1);
  print(jod10==jodd10); // true false
  jod10.add(1);
  jod10.add(-20);
  jodd10.subtract(9.95);
  jodd10.subtract(0.01);
  Money jod1 = Money(1, 'JOD', 0.71);
  jod1.subtract(0.95);
  jod1.subtract(5);
  jod1.subtract(0.05);
  Money jodd5 = Money(5, 'JOD', 0.71);
  jodd5.subtract(10);
  print(jodd5.toString());



  // compare between 3 currencies in doller

  // for example 5 jod is equal to 7.08 doller
  Money qar=Money(20, 'QAR', 0.27);
  print(qar.convertToUsd());
  Money jod=Money(3.85, 'JOD', 0.71);
  print(jod.convertToUsd());
  print(jod==qar); // false
}
