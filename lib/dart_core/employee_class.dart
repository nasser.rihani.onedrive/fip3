class Employee {
  String name;
  double age;
  double salary;
  String position;
  int numOfExperience;
  int numOfVacations;
  String _uniCert; // private
  Employee(
      this.name, this.age, this.salary, this.position, this.numOfExperience) {
    setNumOfVacationsDependOnNumOfExperience();
  }
 // getter and setter
  void setUniCert(String newCert){
    _uniCert=newCert;
  }
  String getUniCert(String position){
    if(position.contains('Mobile'))
      return "EMPTY";
    return _uniCert;
  } // method getter
  // alt + insert to generate methods

  String get uniCert{
    return _uniCert;
  }
  set uniCert(String value) {
    _uniCert = value;
  }

  void setNumOfVacationsDependOnNumOfExperience() {
    if (numOfExperience >= 3) {
      numOfVacations = 21;
    } else {
      numOfVacations = 14;
    }
  }
 // to rename any filed press shift + fn(ctrl) + F6 
  void addOneYearExp({double bonus = 20, String newPosition = ''}) {
    age++;
    numOfExperience++;
    setNumOfVacationsDependOnNumOfExperience();
    salary += bonus;
    if (newPosition.isNotEmpty) {
      position = newPosition;
    }
  }

  void printData() {
    print('the name is $name');
    print('the age is $age');
    print('the salary is $salary');
    print('the position is $position');
    print('the numOfExperience is $numOfExperience');
    print('the numOfVacations is $numOfVacations');
  }
}

void main() {
  Employee tala = Employee(
    "Tala",
    22,
    1500,
    "Mid-level Mobile Developer",
    1,
  );
  tala.printData();
  tala.addOneYearExp(bonus: 50, newPosition: 'Senior Mobile Developer');
  tala.printData();
  tala.addOneYearExp(bonus: 150);
  tala.printData();
  tala.salary=1800;
  tala._uniCert='CS';
  print(tala._uniCert);
}
