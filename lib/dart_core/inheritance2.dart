import 'package:fip3/dart_core/singelton_dp.dart';

import 'inheritance.dart';

abstract class Shape {
  double calc(); // abstract method has no implementation
  void printHello() {
    // Non abstract
    print('Hello');
  }
}

class Rectangle extends Shape {
  @override // annotation
  double calc() {
    return 10;
  }

  @override
  void printHello() {
    // Non abstract
    print('Hello Rectangle');
  }
}

void main() {
  Rectangle rec = Rectangle();
  // Shape shape = Shape();
  // shape.calc();
  print(rec.calc());

  Person ashraf = Person(
      name: 'Asharf',
      ssn: '9971015582',
      idNumber: '4654654',
      dateOfBirth: '645654654',
      age: 20);
  PersonHelper personHelper=PersonHelper(); // new object @456
  print(personHelper.isJorordanianCitizen(ashraf));
  // print(PersonHelper.shared.isJorordanianCitizen(ashraf));
}
