void main() {
  // List arrays stack queue linkedlist map    ---> collections

  int ahmadMark = 80;
  int samrMark = 50;
  int aliMark = 50;
  int TalaMark = 50;
  int HalaMark = 50;

  // computerScienceGroupAMarks  [80,50,40,70,100,99]
  List<int> computerScienceGroupAMarks = [80, 50, 40, 70, 100, 50, 99]; //
  // List <data type > variable name ; deleration  null means the variable does not have a value
  // List <data type > variable name =[]; empty
  // for loop
  print(computerScienceGroupAMarks[0]); // 80
  print(computerScienceGroupAMarks[3]); // 70

  print(computerScienceGroupAMarks.length); // length -1
  print('marks******************');
  for (int index = 0; index < computerScienceGroupAMarks.length; index++) {
    // 0-5
    print(computerScienceGroupAMarks[index]);
  }

  print(computerScienceGroupAMarks.first); // 80
  print(computerScienceGroupAMarks.last); // 99
  print(computerScienceGroupAMarks.isEmpty); // false
  print(computerScienceGroupAMarks.isNotEmpty); // true
  print(computerScienceGroupAMarks.indexOf(50)); // 1
  print(computerScienceGroupAMarks.lastIndexOf(50)); // 5
  print(computerScienceGroupAMarks.contains(0)); // false
  print(computerScienceGroupAMarks);
  computerScienceGroupAMarks = computerScienceGroupAMarks.reversed.toList();
  print(computerScienceGroupAMarks);

  int counter = 0;
  for (int index = 0; index < computerScienceGroupAMarks.length; index++) {
    if (computerScienceGroupAMarks[index] > 75) {
      counter++;
    }
  }
  print('number of digits that greater than 70 is $counter');
  counter = 0;
  computerScienceGroupAMarks.forEach((element) {
    if (element > 70) counter++;
  });
  print('number of digits that greater than 70 is $counter');

  computerScienceGroupAMarks.add(60);
  computerScienceGroupAMarks.add(88);
  computerScienceGroupAMarks.addAll([22, 55, 93]);
  print(computerScienceGroupAMarks);

  // find max second max min second min
  int max = computerScienceGroupAMarks.first;
  computerScienceGroupAMarks.forEach((element) {
    if (element > max) max = element;
  });
  print(max);
  int secondMax = computerScienceGroupAMarks.first;
  computerScienceGroupAMarks.forEach((element) {
    if (element > secondMax && element < max) secondMax = element;
  });
  print(secondMax);
//[]
  List<String> names = [
    'anas',
    'nasser',
    'ashraf',
    'haytham',
    'abdullah',
    'yazan',
    'ahmad',
    'mohammed',
    'ayla'
  ];
  names.add('hala');
  names.add('tala');
  print(names);
  names.addAll(["'sami", "saaed", "rama"]);
  print(names);
  names.remove('tala');
  print(names);
  names.removeAt(1);
  print(names);
  names.removeLast();
  print(names);
  names.setAll(0, ['feras', 'hashim']);
  print(names);
  names.setRange(0, 2, ["anas", "ashraf", "sami"]); // 0 1
  print(names);

  List<String> namesGreaterThan5 =
      names.where((element) => element.length > 5).toList();
  print(namesGreaterThan5);
  List<String> namesStartsWithF =
      names.where((element) => element.startsWith("a")).toList(); // filter
  print(namesStartsWithF);
  String nameStartWithA =
      names.firstWhere((element) => element.startsWith("a"));
  names.removeWhere((element) => element.endsWith('a'));
  print(names);

  // map
  // key value  "anas" : 30   "nasser" : 25
  // Map <key data type , value data type>

  List<String> names2 = [];
  names.add("ahmad");
  List<int> ages2 = [];
  //object
  //
  Map<String, int> fip3 = {
    "Ashraf": 28, // item
    "Ashraf": 29,
    "mohammad": 23,
    "tala": 22,
    "hala": 25, // "sami" : 25
  }; // init
  print("*****************************");
  print(fip3);
  fip3.putIfAbsent("sami", () => 25); // added successfully
  int samiAge = fip3.putIfAbsent("sami", () => 100);
  print(samiAge);
  print(fip3);
  print(fip3.isEmpty);
  print(fip3.isNotEmpty);
  print(fip3['sami']); // key ===> value
  print(fip3['sami2']); // key ===> value
  if (fip3.containsKey("sami2")) {
    print(fip3['sami2']);
  } else {
    print('the name does not exist');
  }
  print(fip3.containsValue(22));
  print(fip3.length);
  // fip3.updateAll((key, value) => 18);
  fip3.update("sami", (value) => 35);
  if (fip3.containsValue("sami2")) {
    fip3.update("sami2", (value) => 35);
  }
  print(fip3);
  fip3.remove("sami"); // key
  print(fip3);
  fip3.forEach((key, value) {
    // 3
    print("$key : $value");
  });
  fip3.removeWhere((key, value) => key.length == 8);

  print(fip3);
  List<int> fip3Ages = fip3.values.toList();
  print(fip3Ages);
  List<String> fip3Names = fip3.keys.toList();
  print(fip3Names);
  fip3 = {};
  print(fip3.length);
  Map<String, dynamic> map; // null
  // print(map.length);
  // map.addAll({"anas": "sami"});
  // Map<String, int> students = {
  //   "yazan": 80,
  //   "Ali": 32,
  //   "ahmad": 40,
  //   "osman": 95,
  //   "wa2el": 55,
  //   "maram": 88,
  //   "malak": 75,
  //   "tmara": 15,
  //   "sedra": 8,
  //   "yara": 92
  // };
  // int marksSum = 0;
  // students.forEach((key, value) {
  //   marksSum += value;
  // });
  // int avg = marksSum * students.length;
  // students.forEach((key, value) {
  //   if (value >= 50)
  //     print("student $key is passed with mark $value");
  // });
  // students.forEach((key, value) {
  //   if (value >= 50)
  //     print("student $key is Not passed with mark $value");
  // });
  // students.removeWhere((key, value) => value<50);
  // students.updateAll((key, value) => value+5);
  // print(students);

  // map { "anas" : 50 , "sami" : 80 , "moh" : 95 , "sara" : 88}
  // sum avg
  // print(passed)
  // print(not passed)
  // remove all the students  (not passed)
  // add 5 marks for all students (passed)
  Map<String, int> students = {
    "yazan": 80,
    "Ali": 32,
    "ahmad": 47,
    "osman": 95,
    "wa2el": 55,
    "maram": 88,
    "malak": 75,
    "tmara": 45,
    "sedra": 8,
    "yara": 92
  };
  int marksSum = 0;
  students.forEach((key, value) {
    marksSum += value;
  });
  double avg = marksSum / students.length;
  print(avg);
  students.forEach((key, value) {
    if (value >= 50)
      print('the student $key is passed with mark $value');
    else
      print('the student $key is Not passed with mark $value');
  });
  // students.removeWhere((key, value) => value < 50);
  // students.updateAll((key, value) => value + 5); // lambda expression
  print(students);

  // write a simple code to
  // print the student name that have the
  // largest mark and the lower mark
  //
  // add 5 marks for all students that may be passed
  // if there marks between 45 < 50

  int stuMax = students['yazan'];
  String maxName = 'yazan';
  int stuMin = students['yazan'];
  String minName = 'yazan';
  students.forEach((key, value) {
    if (value > stuMax) {
      stuMax = value;
      maxName = key;
    }
    if (value < stuMin) {
      stuMin = value;
      minName = key;
    }
  });
  print(maxName);
  print(minName);

  students.forEach((key, value) {
    if (value >= 45 && value < 50) students.update(key, (value) => value + 5);
  });
  print(students);
  // map <string, int>   output max or min  int

  print(findMaxFromMap(students));
  Map<String, int> stundentsClassB = {
    "wa2el": 55,
    "maram": 88,
    "malak": 75,
  };
  print(findMaxFromMap(stundentsClassB));
  print(findMinNameFromMap(students));
  print(findMinNameFromMap(stundentsClassB));
  printValue(25);
  // printValue(30);
  print('the value is 25');
  // try catch
  // handled exception
  List<String> myNames; // null
  // print(myNames[0]);
  // print(myNames.first);

  try {
    print(myNames[0]);
    print(myNames.first);
    // expected error
  } catch(_){
    // handling error
    print('error in list');
  }finally{
    print('finally called');
  }
  // io streaming
}

// return type (int)  method name (//parameters/ arguments (input)) {
// my code
// }

int findMaxFromMap(Map<String, int> map) {
  int max = 0;
  map.forEach((key, value) {
    if (value > max) {
      max = value;
    }
  });
  return max;
}
String findMinNameFromMap(Map<String,int> map){ // function
  int min=100;
  String name='';
  map.forEach((key, value) {
    if(value<min){
      min=value;
      name=key;
    }
  });
  return name;
  print('hello'); //dead code
}

// data type  name (parameter/arguments) {
//  return datatype;
// }

 // print( ' the value is $value'

void printValue(int data){ // 25
  print('the value is $data');
  for(int i=0;i<100;i++) {
    if(i==data){
      return;
    }
    print('hello everyone $i');
  }
  print('completed');
}
