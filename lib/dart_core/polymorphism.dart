class Shape {
  double height;
  double weight;

  Shape(this.height, this.weight);

  void draw() {
    print('Shape is darwing');
  }
}

class Rectangle extends Shape {
  Rectangle(double height, double weight) : super(height, weight);
@override
void draw(){
  print('Rectangle is Drawing');
}
}
class Triangle extends Shape {
  Triangle(double height, double weight) : super(height, weight);
  @override
  void draw(){
    print('Triangle is Drawing');
  }
}

main() {
  Shape shape = new Shape(5,10);
  shape.draw(); // shape is drawing

  Rectangle rec = Rectangle(5,10);
  rec.draw(); // Rectangle is drawing

  Shape rectangle = Rectangle(10,15); // polymorphism
  Triangle triangle=Triangle(20, 50);
  // rectangle.draw(); // dependency enjection   design pattern
  printRectangle(rectangle);
  printTriangle(triangle);
  printShape(triangle);
  printShape(rectangle); //dependency enjection
}
printRectangle(Rectangle rectangle){
  rectangle.draw();
}
printTriangle(Triangle triangle){
  triangle.draw();
}
printShape(Shape shape){
  shape.draw(); //
}
