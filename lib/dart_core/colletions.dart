void main() {
  // list map

  int age = 10; // 10
  int xAge = 30;
  int naserAge = 22;
  int mohammedAge = 25;
  int samiAge = 25;

  double avg = (age + xAge + naserAge + mohammedAge + samiAge) / 5;

  List<int> ages = [0, 10, 20, 30, 40, 50]; // generic type
  // List<String> names=['anas','ahmad','sami'];

  // List <type> variablename ; decleration
  // List <type> variablename =[]; init
  // List<String>words=[]; // empty
  // List<String> c; // null
  for (int index = 0; index < ages.length; index++) {
    print('index $index = ' + ages[index].toString());
  }

  print(ages.length); // 5
  print(ages.first); // first element 10
  print(ages[0]);
  print(ages.last); // last element 50
  print(ages[ages.length - 1]);
  print(ages.isNotEmpty); // true
  print(ages.isEmpty); // false
  print(ages.contains(20)); // true
  print(ages.indexOf(30)); //  3 , if not exist -1
  print(ages.lastIndexOf(30)); // 3
  ages = ages.reversed.toList();
  print(ages);

  int counter = 0;
  for (int index = 0; index < ages.length; index++) {
    if (ages[index] > 30) counter++;
  }
  print('number of digits that greater than 30 is $counter');
  List<int> agesGreaterThan30 = ages.where((element) => element > 30).toList();
  print('number of digits that greater than 30 is ' +
      agesGreaterThan30.length.toString());
  print('number of digits that greater than 30 is ${agesGreaterThan30.length}');
  print(agesGreaterThan30);
  bool isNumGreaterThan50 = false;
  ages.forEach((element) {
    if (element > 50) {
      isNumGreaterThan50 = true;
    }
  });
  if (isNumGreaterThan50) print(ages.firstWhere((element) => element > 50));
  ages.add(100);
  ages.add(80);
  ages.add(0);
  ages.addAll([10, 20, 20, -80, -100, 50, 40]);
  print(ages);
  ages.addAll(agesGreaterThan30);
  bool isNumGreaterThan200 = ages.any((element) => element > 200);
  // ages.clear(); // to remove all data []
  print(ages.isEmpty); // true
  print(isNumGreaterThan200);

  // write a simple code to find the max number inside the ages list
  int max = ages.first;
  ages.forEach((element) {
    if (element > max) max = element;
  });

  print(max);
  int secondMax = ages.first;
  ages.forEach((element) {
    if (element > secondMax && element < max) secondMax = element;
  });
  print('second max $secondMax');
  /////////////////////

  int min = ages.first;
  ages.forEach((element) {
    if (element < min) {
      min = element;
    }
  });
  print(min);

  int secMin = ages.first;
  ages.forEach((element) {
    if (element < secMin && element > min) secMin = element;
  });
  print("second min= $secMin");

  secondMax = ages[0];
  for (int index = 0; index < ages.length; index++) {
    if (ages[index] == max) continue;
    if (ages[index] > secondMax) secondMax = ages[index];
  }
  print("second max is $secondMax"); // alt + ctrl + l to reformat your code

  List<String> names = [
    'anas',
    'nasser',
    'ashraf',
    'haytham',
    'abdullah',
    'yazan',
    'ahmad',
    'mohammed',
    'ayla'
  ];

  List myList = ["anas", 5, 18.5, false];
  myList.add(15);
  myList.add("anas");
  print(myList);
  // write a simple code to print all names that
  // starts with a and their length is less than 5

  names.add('hala');
  names.add('tala');
  names.addAll(['muath', 'ibrahim']);
  names.remove('ayla'); // remove ayla directly
  names.removeAt(0); // remove element by index
  names.removeLast(); // remove last element
  names.removeRange(0, 2); // remove range from 0 to 2 (index 2 not included)
  names.removeWhere((element) =>
      element.length > 8); // remove all element depends the condition
  print(names);
  names.setAll(0, ['anas', 'nasser']);
  names.addAll(['anas', 'nasser']);
  names.setRange(0, 2, ['ali', 'sami', 'ali']);
  print(names);
  names[5] = 'ahmad';
  print(names);

  // write a simple code to calculate the sum and the avg for degrees
  // note the list should be double

  // write a simple code to print the degrees that greater than 50
  // int data = stdin.readByteSync();
  // String word= stdin.readLineSync();

  // print(data);
  // print(word);

  Map<String, int> map = {};
  map.putIfAbsent("anas", () => 30);
  map.putIfAbsent("ahmad", () => 50);
  map.putIfAbsent("nasser", () => 70);
  map.putIfAbsent("nasser", () => 70);
  print(map);
  print(map.length);
  print(map.isNotEmpty);
  print(map.isEmpty);
  map.remove("nasser");
  print(map);
  map.removeWhere((key, value) => key.length == 4);
  print(map);
  map.addAll({"anas": 90, "nasser": 70});
  print(map);
  map.forEach((key, value) {
    print('key is $key and value is $value');
  });
  map.update("anas", (value) => 10);
  print(map);
  print(map.containsKey("nasser"));
  print(map.containsValue(20));
  map.updateAll((key, value) => 20);
  print(map);
  // map.clear();
  print(map);
  List<String> mapNames = map.keys.toList();
  List<int> mapAges = map.values.toList();
  print(mapNames);
  print(mapAges);
  Map myMap = Map.fromIterable(names, key: (v) => v, value: (a) => 20);
  Map myMap2 = names.asMap();
  print(myMap);
  print(myMap2);

  print(map["anas"]);
  // {  "anas" : 50 , "omar" : 70 .... } write a simple code to find the oldest and the youngest student in the list
}
