void main() {
  String inter = 'international';
  print(inter.substring(inter.trim().length - 5));
  print('***********************************');

  int x = 20;
  int y = 30;
  int z = 30;

  if (x > y && x > z) {
    print('x is the largest $x');
    // print('x is the largest'+x.toString());
  } else if (y > x && y > z) {
    print('y is the largest $y');
  } else if (z > x && z > y) {
    print('z is the largest $z');
  } else {
    print('the numbers are equivalents');
  }

  print('****************************');

  String word = 'levelee'; // palindrome string
  print(word[0]); // l
  print(word[3]); // e

  String reverseWord = ''; // leve

  for (int index = 0; index < word.length; index++) {}

  for (int index = word.length - 1; index >= 0; index--) {
    reverseWord = reverseWord + word[index];
  }

  print(word);
  print(reverseWord);
  // if (word == reverseWord)
  //   print('the string $word is palindrome');
  // else
  //   print('the string $word is NOT palindrome');

  // print((word == reverseWord)
  //     ? 'the string' + word.toString() + ' is palindrome'
  //     : 'the string $word is NOT palindrome'); //

  //  l e v e  l   5  2.5=2
  // mid 2
  // start 0
  // end 4
  // 2 pointers

  int mid = (word.length / 2).floor().toInt(); // 2
  int leftPointer = mid - 1; // 1 0
  int rightPointer = mid + 1; // 3 4
  bool wordIsPalindrome = true;
  while (leftPointer >= 0 && rightPointer <= word.length - 1) {
    if (word[leftPointer] != word[rightPointer]) {
      print('the string is not palindrome');
      wordIsPalindrome = false;
      break;
    }
    leftPointer--;
    rightPointer++;
  }
  if (wordIsPalindrome) print('the string is palindrome');

  String num1 = 'a100';
  String num2 = '20';

  // string to int  casting   exceptions  1051156
  int rNum1 = int.tryParse(num1)??18; // null
  int rNum2 = int.tryParse(num2)??18;
  String a; // decleration
  if (rNum1 > rNum2)
    print('num1 > num2');
  else if (rNum2 > rNum1)
    print('num2 > num1');
  else
    print('the numbers are equals');


}
