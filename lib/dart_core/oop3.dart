
class Car {
  CarType type;
  String model;
  String name;
  List<Wheel> wheels;
  Motor motor;
  // Color color;

  Car({this.type, this.model, this.name, this.wheels, this.motor,});

  @override
  String toString() {
    return 'Car{type: $type, model: $model, name: $name, wheels: $wheels, motor: $motor}';
  }



}

class Wheel {
  double thickness;
  double long;

  Wheel(this.thickness, this.long);

  @override
  String toString() {
    return 'Wheel{thickness: $thickness, long: $long}';
  }
}

class Motor {
  int cc;
  int hourses;
  String type;

  Motor({this.cc, this.hourses, this.type});

  @override
  String toString() {
    return 'Motor{cc: $cc, hourses: $hourses, type: $type}';
  }
}

enum CarType {
  hyundai,
  kia,
  mercedes
  // dealing with enum ,extension
}

void main() {
  Car car = Car(
      type: CarType.mercedes,
      name: 'Mercedes g-class',
      model: 'e200',
      motor: Motor(type: 'GDI', cc: 2000, hourses: 300));
  Car car2 = Car(
      type: CarType.mercedes,
      name: 'Mercedes g-class',
      model: 'e200',
      motor: Motor(type: 'GDI', cc: 2000, hourses: 300));
    print(car.type==CarType.mercedes); // true false

  Car car3 = Car(
      type: CarType.mercedes,
      name: 'Mercedes g-class',
      model: 'e200',
      motor: Motor(type: 'GDI', cc: 2000, hourses: 300));
  print(car.type==CarType.mercedes); // true

}

// inheritance ,design pattern (singelton , factory )
