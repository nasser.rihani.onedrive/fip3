import 'package:fip3/dart_core/singelton_dp.dart';

class Person { // model object bean
  // parent super class
  String name;
  int age;
  String idNumber;
  String dateOfBirth;
  String ssn;

  Person({this.name, this.age, this.idNumber, this.dateOfBirth, this.ssn});

  @override
  String toString() {
    return 'Person{name: $name, age: $age, idNumber: $idNumber, dateOfBirth: $dateOfBirth, ssn: $ssn}';
  }
  // from json to json
}

class Human extends Student {}

class Student extends Person {
  // child
  String studentNumber;
  String major;
  String mark;

  Student(
      {String name,
      int age,
      String idNumber,
      String dateOfBirth,
      String ssn,
      this.studentNumber,
      this.major,
      this.mark})
      : super(
            name: name,
            age: age,
            dateOfBirth: dateOfBirth,
            idNumber: idNumber,
            ssn: ssn);

  @override
  String toString() {
    return 'Student{studentNumber: $studentNumber, major: $major, mark: $mark}' +
        super.toString();
  }
}

void main() {
  Person ahmad = Person(
      name: 'Ahmad',
      age: 35,
      idNumber: '9921445623',
      dateOfBirth: '05/11/1997',
      ssn: '15616515');
  Student ali = Student(
      name: 'Ali',
      age: 35,
      idNumber: '9921445623',
      dateOfBirth: '05/11/1997',
      ssn: '9981014331',
      major: 'Cs',
      mark: '88',
      studentNumber: '516165156165');
  printData(ahmad);
  printData(ali);
  print(ali.toString());
  ali.studentNumber='123456789';
  ali.age++;

 // new object @123
  PersonHelper helper=PersonHelper(); // new object @address
  print(helper.isJorordanianCitizen(ali));


}

void printData(Person person) {
  print(person.toString());
}


