
import 'inheritance.dart';

class PersonHelper {

 static PersonHelper shared =PersonHelper._private(); // new object from private constructor

 factory PersonHelper(){ // constructor
   return shared;
 }
  PersonHelper._private(); // constructor

  int calcAge(String dateOfBirth) {
    return 10;
  }

  bool isJorordanianCitizen(Person person) {
    return person.ssn.contains('998');
  }
}
