
abstract class Person {
  String name;
  int age;

  Person(this.name, this.age);
}

class Employee extends Person {
  double salary;
  String position;

  Employee(this.salary, this.position, String name, int age) : super(name, age);
}

void main() {
  Employee employee = Employee(100, "Developer", "Anas", 25);
  print(employee.age);
  print(employee.name);
}

