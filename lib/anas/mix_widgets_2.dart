import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:regexpattern/regexpattern.dart';
class MixWidgets2 extends StatefulWidget {
  const MixWidgets2({Key key}) : super(key: key);

  @override
  State<MixWidgets2> createState() => _MixWidgets2State();
}

class _MixWidgets2State extends State<MixWidgets2> {
  final TextEditingController _userNameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final String _username = '';
  int counter = 0;
  bool _isObscureText = true;
  final FocusNode _usernameFocus=FocusNode();
  final FocusNode _passwordFocus=FocusNode();
  final FocusNode _emailFocus=FocusNode();
  String _usernameErrorText='';
  String _emailErrorText='';
  @override
  Widget build(BuildContext context) {
    print(++counter);
    return SafeArea(
        child: Scaffold(
      body: Container(
        margin: EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          children: [
            const Text('Username'),
            TextField(
              decoration: InputDecoration(
                border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(12)),
                focusedBorder:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(12)),
                errorBorder:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(12)),
                errorText: _usernameErrorText,
                enabled: true,
                fillColor: Colors.yellow,
                filled: false,
                // hintText: 'username',
                // hintStyle: TextStyle(fontSize: 25, color: Colors.red),
                labelText: 'Username',
                labelStyle: const TextStyle(color: Colors.black),
                prefixIcon: const Icon(Icons.person),
                suffixIcon: const Icon(Icons.remove_red_eye),
                icon: const Icon(
                  Icons.star,
                  color: Colors.pink,
                ),
              ),
              textInputAction: TextInputAction.next,
              controller: _userNameController,
              focusNode: _usernameFocus,
              inputFormatters: [

              ],
              onSubmitted: (String value){
              FocusScope.of(context).requestFocus(_passwordFocus);
                _passwordFocus.requestFocus();
              },
              // onChanged: (String value){
              //   setState(() {
              //     _username=value;
              //   });
              // },
            ),
            TextField(
              decoration: InputDecoration(
                  labelText: 'email',
                  errorText: _emailErrorText,
                  suffixIcon: IconButton(
                      icon: Icon(_isObscureText
                          ? Icons.remove_red_eye
                          : Icons.visibility_off),
                      onPressed: () {
                        setState(() {
                          _isObscureText = !_isObscureText;
                        });
                      }),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(14))),
              focusNode: _emailFocus,
              controller: _emailController,
              // obscureText: _isObscureText,
              keyboardType: TextInputType.text,
            ),
            TextField(
              decoration: InputDecoration(
                  labelText: 'Password',
                  suffixIcon: IconButton(
                      icon: Icon(_isObscureText
                          ? Icons.remove_red_eye
                          : Icons.visibility_off),
                      onPressed: () {
                        setState(() {
                          _isObscureText = !_isObscureText;
                        });
                      }),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(14))),
              // focusNode: _passwordFocus,
              obscureText: _isObscureText,
              inputFormatters: [
              ],
            ),
            const Spacer(),
            ElevatedButton(
                onPressed: () {
                  _clearErrorsMessages();
                  if(_userNameController.text.isEmpty){
                      _usernameFocus.requestFocus();
                      setState(() {
                        _usernameErrorText='Please fill username field';
                      });
                  }
                else if(_userNameController.text.length<6){
                    _usernameFocus.requestFocus();
                    setState(() {
                      _usernameErrorText='username must be greater than 5 letters';
                    });
                  }
                  else  if(_emailController.text.isEmpty){
                    _usernameFocus.requestFocus();
                    setState(() {
                      _emailErrorText='Please fill e-mail field';
                    });
                  }
                  // bool emailValid = RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(_emailController.text);
                  else  if(!_emailController.text.isEmail()){
                    _emailFocus.requestFocus();
                    setState(() {
                      _emailErrorText='this email is not valid';
                    });
                  }
                  // else  if(!_emailController.text.pass){
                  //   _emailFocus.requestFocus();
                  //   setState(() {
                  //     _emailErrorText='this email is not valid';
                  //   });
                  // } // textFormField
                },
                child: const Text('Submit'))
          ],
        ),
      ),
    ));
  }

  void hello(String value) {
    // valueChanged
    print(hello);
  }

  void _clearErrorsMessages() {
    setState(() {
      _usernameErrorText='';
      _emailErrorText='';
    });
  }
}

