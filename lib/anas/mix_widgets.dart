import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class MixWidgets extends StatefulWidget {
  const MixWidgets({Key key}) : super(key: key);

  @override
  State<MixWidgets> createState() => _MixWidgetsState();
}

class _MixWidgetsState extends State<MixWidgets> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset(
                'assets/apple/apple_logo.jpg',
                alignment: Alignment.center,
                fit: BoxFit.cover,
                width: 100,
                height: 100,
              ),
              // Image.network(
              //   'https://1000logos.net/wp-content/uploads/2016/10/Apple-Logo.png',
              //   width: 400,
              //   height: MediaQuery.of(context).size.height * 0.1,
              // ),
              CachedNetworkImage(
                imageUrl: "http://via.placeholder.com/350x150",
                placeholder: (context, url) =>
                    CircularProgressIndicator(color: Colors.pink),
                errorWidget: (context, url, error) => Icon(Icons.error),
              ),
              // Image.memory(base64Decode(apple_img)),
              // Image.asset('assets/samsung/apple_eats_samsung.jpg',),
              FlutterLogo(
                size: 50,
              ),
              SizedBox(
                height: 100,
              ),
              SizedBox(
                width: 200,
                height: 50,
                child: ElevatedButton(
                  onPressed: () {
                    print('Hello');
                  },
                  child: Padding(
                    padding: EdgeInsets.only(right: 10),
                    child: Text(
                      'Ok',
                      style: TextStyle(),
                    ),
                  ),
                  style: ElevatedButton.styleFrom(
                    primary: Colors.orange,
                    // maximumSize: Size(20,10),
                  ),
                ),
              ),
              TextButton(
                onPressed: () {},
                child: Text('Ok'),
                style: TextButton.styleFrom(primary: Colors.green),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
