import 'package:flutter/material.dart';

class FutureExample extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder<List<InfoData>>(
        future: _fillData(),
        builder: (context, snapShot) {
          if (snapShot.hasError) {
            return const Center(
              child: Text('Something went wrong please contact us'),
            );
          }
          if (!snapShot.hasData) {
            return const Center(
              child: CircularProgressIndicator(
                color: Colors.blue,
              ),
            );
          } else {
            List<InfoData> data=snapShot.data;
            return Center(
                child: ListView.builder(
              itemBuilder: (context, index) {
                return SizedBox(
                  height: 50,
                  child: Center(
                    child: Text(data[index].name + data[index].id.toString()),
                  ),
                );
              },
              itemCount: data.length,
            ));
          }
        },
      ),
    );
  }

  Future<List<InfoData>> _fillData() async {
    return Future.delayed(Duration(seconds: 10), () {
      return [
        InfoData(name: 'Anas', id: 1),
        InfoData(name: 'Anas', id: 1),
        InfoData(name: 'Anas', id: 1),
        InfoData(name: 'Anas', id: 1),
        InfoData(name: 'Anas', id: 1),
        InfoData(name: 'Anas', id: 1),
        InfoData(name: 'Anas', id: 1),
        InfoData(name: 'Anas', id: 1),
        InfoData(name: 'Anas', id: 1),
        InfoData(name: 'Anas', id: 1),
      ];
    });
  }
}

class InfoData {
  String name;
  int id;

  InfoData({this.name, this.id});
}
