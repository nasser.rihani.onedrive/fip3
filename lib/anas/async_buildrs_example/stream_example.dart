import 'package:flutter/material.dart';

class StreamExample extends StatelessWidget {
  int _count = 0;
  StreamExample({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          StreamBuilder<int>(
            stream: _fillData(),
            builder: (context, snapShot) {
              if (snapShot.hasError) {
                return const Center(
                  child: Text('Something went wrong please contact us'),
                );
              }
              if (!snapShot.hasData) {
                return const Center(
                  child: CircularProgressIndicator(
                    color: Colors.blue,
                  ),
                );
              } else {
                int second = snapShot.data;
                return Center(
                    child: Text(
                  second.toString(),
                  style: TextStyle(fontSize: 30),
                ));
              }
            },
          ),
          Text('HEllo')
        ],
      ),
    );
  }

  Stream<int> _fillData() async* {
    while (true) {
      await Future.delayed(const Duration(seconds: 1));
      yield _count++;
    }
  }
}

class InfoData {
  String name;
  int id;

  InfoData({this.name, this.id});
}
