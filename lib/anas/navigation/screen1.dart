import 'package:fip3/anas/navigation/screen2.dart';
import 'package:flutter/material.dart';

class Screen1 extends StatelessWidget {
  const Screen1({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
          appBar: AppBar(),
          body: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  InkWell(child: Text('Screen1',), onTap: () {
                    // Navigator.of(context).push(
                    //     MaterialPageRoute(builder: (context) => Screen2()));
                    Navigator.of(context).pushNamed('/screen2');
                    // Navigator.of(context).pop();

                  }),
                ],
              )),
        ));
  }
}
