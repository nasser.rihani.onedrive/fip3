import 'package:flutter/material.dart';

class Screen2 extends StatelessWidget {
  const Screen2({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(),
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          InkWell(
            child: Text('Screen2'),
            onTap: () {
              // Navigator.of(context).pushAndRemoveUntil(
              //   MaterialPageRoute(builder: (context) => Screen3()),
              //   (route) => false,
              // );
              // Navigator.pushNamedAndRemoveUntil(context, '/screen3', (route) => false);
              Navigator.of(context).pushNamedAndRemoveUntil('/screen3', (route) => false);
              // Navigator.of(context).pop();
            },
          ),
        ],
      )),
    ));
  }
}
