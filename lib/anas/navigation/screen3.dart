import 'package:flutter/material.dart';

class Screen3 extends StatelessWidget {
  const Screen3({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
          body: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  InkWell(child: Text('Screen3'),onTap: (){
                    Navigator.of(context).pop();
                  },),
                ],
              )),
        ));
  }
}
