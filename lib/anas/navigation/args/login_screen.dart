import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  bool _isObscureText = false;
  int _group = -1;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              TextField(
                decoration: InputDecoration(
                    labelText: 'email',
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(14))),
                controller: _emailController,
                // obscureText: _isObscureText,
                keyboardType: TextInputType.text,
              ),
              TextField(
                decoration: InputDecoration(
                    labelText: 'Password',
                    suffixIcon: IconButton(
                        icon: Icon(_isObscureText
                            ? Icons.remove_red_eye
                            : Icons.visibility_off),
                        onPressed: () {
                          setState(() {
                            _isObscureText = !_isObscureText;
                          });
                        }),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(14))),
                controller: _passwordController,
                // focusNode: _passwordFocus,
                obscureText: _isObscureText,
              ),
              ElevatedButton(onPressed: () {}, child: Text('Login')),
              SizedBox(
                height: 100,
                width: 100,
                child: Card(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12)),
                    margin: EdgeInsets.all(8),
                    color: Colors.red,
                    elevation: 20,
                    shadowColor: Colors.redAccent,child: Center(child: Text('Hello'))),
              ),
              ListTile(
                title: const Text('Radio 1'),
                leading: Radio(
                  value: 1,
                  groupValue: _group,
                  onChanged: (val) {
                    setState(() {
                      _group = val;
                    });
                  },
                ),
              ),
              ListTile(
                title: Text('Radio 2'),
                leading: Radio(
                  value: 2,
                  groupValue: _group,
                  onChanged: (val) {
                    setState(() {
                      _group = val;
                    });
                  },
                ),
              ),
              GestureDetector(
                  child: Text('You dont have an account Register Here'),
                  onTap: () async {
                    Map<String, String> result = await Navigator.of(context)
                        .pushNamed('/register') as Map<String, String>;
                    setState(() {
                      _emailController.text = result['email'];
                      _passwordController.text = result['password'];
                    });
                  }),
            ]),
      ),
    );
  }
}
