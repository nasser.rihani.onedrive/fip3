import 'package:flutter/material.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key key}) : super(key: key);

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  bool _isObscureText = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              TextField(
                decoration: InputDecoration(
                    labelText: 'email',
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(14))),
                controller: _emailController,
                // obscureText: _isObscureText,
                keyboardType: TextInputType.text,
              ),
              TextField(
                decoration: InputDecoration(
                    labelText: 'Password',
                    suffixIcon: IconButton(
                        icon: Icon(_isObscureText
                            ? Icons.remove_red_eye
                            : Icons.visibility_off),
                        onPressed: () {
                          setState(() {
                            _isObscureText = !_isObscureText;
                          });
                        }),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(14))),
                controller: _passwordController,
                // focusNode: _passwordFocus,
                obscureText: _isObscureText,
              ),
              ElevatedButton(onPressed: () {
                // FocusScope.of(context).requestFocus(FocusNode());
                // FocusScope.of(context).unfocus();
                // FocusManager.instance.primaryFocus?.unfocus();
                // Navigator.of(context).pop({
                //   'email':_emailController.text,
                //   'password':_passwordController.text,
                // });
              }, child: Text('Register' )),
            ]),
      ),
    );
  }
}
