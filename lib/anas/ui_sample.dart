import 'package:flutter/material.dart';

class UiSample extends StatelessWidget {
  //convention upper camel case
  const UiSample({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _ui_sample2();
  }

  Widget _ui_sample2() {
    return SafeArea(
        child: Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              Icon(Icons.add),
              Icon(Icons.add),
            ],
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
          ),
          Align(
            child: Icon(Icons.add),
            alignment: Alignment.centerLeft,
          ),
          Center(child: Text('hello')),
          Align(
            child: Icon(Icons.add),
            alignment: Alignment.centerRight,
          ),
          Row(
            children: [
              Icon(Icons.add),
              Icon(Icons.add),
            ],
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
          ),
        ],
      ),
    ));
  }

  Widget _ui_sample1() {
    return SafeArea(
      child: Scaffold(
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                Icon(Icons.add),
                Icon(Icons.add),
              ],
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
            ),
            Center(child: Text('hello')),
            Row(
              children: [
                Icon(Icons.add),
                Icon(Icons.add),
              ],
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
            ),
          ],
        ),
      ),
    );
  }
}
