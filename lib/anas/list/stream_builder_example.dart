import 'dart:async';

import 'package:flutter/material.dart';

class StreamBuilderUi extends StatelessWidget {
  StreamBuilderUi({Key key}) : super(key: key);
  int _count=0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StreamBuilder<int>(
        stream: _getSeconds(),
        builder: (context, snapShot) {
          if (snapShot.hasError) {
            return Container(
              alignment: Alignment.center,
              child: Text('Error while receiving data'),
            );
          }
          if (!snapShot.hasData) {
            return const Center(
              child: CircularProgressIndicator(
                color: Colors.blue,
              ),
            );
          } else {
            return Center(
              child: Text(snapShot.data.toString()),
            );
          }
        },
      ),
    );
  }

  Stream<int> _getSeconds() async* {
    while(true){
      await Future.delayed(const Duration(seconds: 1));
      yield _count++;
    }
  }
}

