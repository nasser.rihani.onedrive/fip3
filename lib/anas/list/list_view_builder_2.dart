import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class ListViewBuilder2 extends StatefulWidget {
  const ListViewBuilder2({Key key}) : super(key: key);

  @override
  State<ListViewBuilder2> createState() => _ListViewBuilder2State();
}

class _ListViewBuilder2State extends State<ListViewBuilder2> {
  List<MyCityModel> cities = [];

  @override
  void initState() {
    cities = _fillData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.amber,
        title: Text('Cities Around the World'),
      ),
      body: Container(
        color: Colors.white,
        child: GridView.builder(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 3,
            crossAxisSpacing: 0.0,
            mainAxisSpacing: 2,
          ),
          itemBuilder: (context, index) {
            return _listViewItemBuilder(context, index);
          },
          itemCount: cities.length,
        ),
      ),
    );
  }

  Container _listViewItemBuilder(BuildContext context, int index) {
    return Container(
      width: MediaQuery.of(context).size.width*0.1,
      height: MediaQuery.of(context).size.height * 0.18,
      margin: EdgeInsets.symmetric(horizontal: 8),
      child: Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
        child: Column(
          children: [
            SizedBox(
              width: 200,
              child: CachedNetworkImage(
                imageUrl: cities[index].imgUrl,
                placeholder: (context, url) {
                  return CircularProgressIndicator();
                },
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  cities[index].cityName,
                  style: TextStyle(
                      color: Colors.black, fontWeight: FontWeight.bold),
                ),
                Text(
                  cities[index].country,
                  style: TextStyle(color: Colors.black),
                ),
                Text(
                  cities[index].populationLabel,
                  style: TextStyle(color: Colors.black),
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  List<MyCityModel> _fillData() {
    return [
      MyCityModel(
          imgUrl:
              "https://velvetescape.com/wp-content/uploads/2011/10/Petra-by-Night-5-1.jpg",
          cityName: 'Petra',
          country: 'Jordan',
          populationLabel: "Population 20 mill"),
      MyCityModel(
          imgUrl:
              "https://dandg.azureedge.net/cache/2/2/5/7/f/a/2257fa9015c73278d8fa06c2ce23f2df6d5531c4.jpg",
          cityName: 'London',
          country: 'Britain',
          populationLabel: "Population 25 mill"),
      MyCityModel(
          imgUrl:
              "https://assets.simpleviewinc.com/simpleview/image/upload/c_fill,h_463,q_50,w_800/v1/clients/vancouverbc/The-City_121bc6b2-ca7f-45be-a877-7e166af5502d.jpg",
          cityName: 'Vancouver',
          country: 'Canada',
          populationLabel: "Population 24 mill"),
      MyCityModel(
          imgUrl:
              "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRKOy6uNbpFHw7zWr8FhQIrKWxsuo65lNTi7A&usqp=CAU",
          cityName: 'New York',
          country: 'USA',
          populationLabel: "Population 14 mill"),
      MyCityModel(
          imgUrl:
              "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQTz7z2Ayon7CstLjIlVobhZDzboCrQ30M-MQ&usqp=CAU",
          cityName: 'Sofia',
          country: 'Bulgaria',
          populationLabel: "Population 8 mill")
    ];
  }
}

class MyCityModel {
  String imgUrl;
  String cityName;
  String country;
  String populationLabel;

  MyCityModel(
      {@required this.imgUrl,
      @required this.cityName,
      @required this.country,
      @required this.populationLabel});
}
