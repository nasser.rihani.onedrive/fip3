import 'dart:async';

import 'package:flutter/material.dart';

class FutureBuilderUi extends StatelessWidget {
  FutureBuilderUi({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder<List<Data>>(
        future: _getData(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.hasError) {
            return Center(
              child: Text('Something went wrong'),
            );
          }
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          List<Data> myData = snapshot.data;
          if(myData.isEmpty)
            return Center(child: Text('you dont have data click here to start!!'),);
          return ListView.builder(
            itemBuilder: (context, index) {
              return Text(myData[index].name);
            },
            itemCount: myData.length,
          );
        },
      ),
    );
  }

  //
  // @override
  // Widget build(BuildContext context) {
  //   return Scaffold(
  //     body: FutureBuilder<List<Data>>(
  //       future: _getListOfData(),
  //       builder: (context, snapShot) {
  //         if (snapShot.hasError) {
  //           return Container(
  //             alignment: Alignment.center,
  //             child: Text('Error while receiving data'),
  //           );
  //         }
  //         if (!snapShot.hasData) {
  //           return const Center(
  //             child: CircularProgressIndicator(
  //               color: Colors.blue,
  //             ),
  //           );
  //         } else {
  //           List<Data> data = snapShot.data;
  //           return ListView.builder(
  //             itemBuilder: (context, index) {
  //               return Container(
  //                 color: Colors.red,
  //                 height: 20,
  //                 child: Text(data[index].name),
  //               );
  //             },
  //             itemCount: data.length,
  //           );
  //         }
  //       },
  //     ),
  //   );
  // }

  Future<List<Data>> _getListOfData() async {
    return Future.delayed(const Duration(seconds: 10), () {
      return [
        Data(id: 1, name: 'Anas'),
        Data(id: 1, name: 'Anas'),
        Data(id: 1, name: 'Anas'),
        Data(id: 1, name: 'Anas'),
        Data(id: 1, name: 'Anas'),
        Data(id: 1, name: 'Anas'),
      ];
    });
  }

  Future<List<Data>> _getData() {
    return Future.delayed(Duration(seconds: 5), () {
      return [
        Data(id: 1, name: 'Anas'),
        Data(id: 1, name: 'Anas'),
        Data(id: 1, name: 'Anas'),
        Data(id: 1, name: 'Anas'),
      ];
    });
  }
}

class Data {
  String name;
  int id;

  Data({this.name, this.id});
}
