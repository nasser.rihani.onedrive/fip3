import 'package:flutter/material.dart';

class AwaitExample extends StatefulWidget {
  const AwaitExample({Key key}) : super(key: key);

  @override
  State<AwaitExample> createState() => _AwaitExampleState();
}

class _AwaitExampleState extends State<AwaitExample> {
  List<int> numbers = [];

  @override
  void initState() {
    _fillNumbers();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: numbers.length == 0
          ? Center(
              child: CircularProgressIndicator(
              color: Colors.blue,
            ))
          : ListView.builder(
              itemBuilder: (context, index) {
                return SizedBox(
                  height: 40,
                  child: Center(
                    child: Text(numbers[index].toString()),
                  ),
                );
              },
              itemCount: numbers.length,
            ),
    );
  }

  void _fillNumbers() async {
    numbers = await _getNumbersFromDataBase();
    print('hello');
    setState(() {});
  }

  Future<List<int>> _getNumbersFromDataBase() async {
    return Future.delayed(Duration(seconds: 6), () {
      return [1, 2, 3, 4, 5, 6];
    });
  }

  List<int> getNumbers(){
    return [1,2,3];
  }

}
