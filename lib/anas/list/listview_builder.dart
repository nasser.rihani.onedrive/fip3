import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class ListViewExample extends StatefulWidget {
  const ListViewExample({Key key}) : super(key: key);

  @override
  _ListViewExampleState createState() => _ListViewExampleState();
}

class _ListViewExampleState extends State<ListViewExample> {
  List<MyCityModel> cities = [];

  @override
  void initState() {
// get data from databse
    cities = _fillData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.amber,
          title: Text('Cities Around the World'),
        ),
        body: Container(
            color: Colors.white,
            child: ListView.builder(
              // gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              //   crossAxisCount: 2,
              //   crossAxisSpacing: 1.0,
              //   mainAxisSpacing: 5.0,
              // ),
              itemBuilder: (BuildContext context, int index) {
                return GestureDetector(child: _getCardView(cities[index]),onTap: (){
                  print(cities[index].cityName);
                },);
              },
              itemCount: cities.length,
              //   },),
            )));
  }

  Widget _getCardView(MyCityModel model) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * 0.18,
      margin: EdgeInsets.symmetric(
          horizontal: MediaQuery.of(context).size.width * 0.01,
          vertical: MediaQuery.of(context).size.height * 0.004),
      color: Colors.white,
      child: Card(
        elevation: 8,
        shadowColor: Colors.black,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
              width: width * 0.06,
            ),
            CachedNetworkImage(
              imageUrl: model.imgUrl,
              placeholder: (context, url) =>
                  CircularProgressIndicator(color: Colors.red),
              errorWidget: (context, url, error) => Icon(Icons.error),
              imageBuilder:
                  (BuildContext context, ImageProvider imageProvider) {
                return Container(
                  height: height * 0.2,
                  width: width * 0.45,
                  decoration: BoxDecoration(
                      image: DecorationImage(image: imageProvider)),
                );
              },
            ),
            SizedBox(
              width: width * 0.04,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  model.cityName,
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.w600,
                      fontSize: 20),
                ),
                SizedBox(
                  height: height * 0.01,
                ),
                Text(
                  model.country,
                  style: TextStyle(
                      color: Colors.grey,
                      fontWeight: FontWeight.normal,
                      fontSize: 18),
                ),
                Text(
                  model.populationLabel,
                  style: TextStyle(
                      color: Colors.grey,
                      fontWeight: FontWeight.normal,
                      fontSize: 18),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _getGridView(MyCityModel model) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Container(
      width: width * 0.4,
      height: width * 0.4,
      margin: EdgeInsets.symmetric(
          horizontal: width * 0.01, vertical: height * 0.004),
      color: Colors.white,
      child: Card(
        elevation: 8,
        shadowColor: Colors.black,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            CachedNetworkImage(
              imageUrl: model.imgUrl,
              placeholder: (context, url) =>
                  CircularProgressIndicator(color: Colors.red),
              errorWidget: (context, url, error) => Icon(Icons.error),
              imageBuilder:
                  (BuildContext context, ImageProvider imageProvider) {
                return Container(
                  height: width * 0.35,
                  width: width * 0.35,
                  decoration: BoxDecoration(
                      image: DecorationImage(image: imageProvider)),
                );
              },
            ),
            Text(
              model.cityName,
              style: const TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w600,
                  fontSize: 20),
            ),
          ],
        ),
      ),
    );
  }

  List<MyCityModel> _fillData() {
    return [
      MyCityModel(
          imgUrl:
          "https://velvetescape.com/wp-content/uploads/2011/10/Petra-by-Night-5-1.jpg",
          cityName: 'Petra',
          country: 'Jordan',
          populationLabel: "Population 20 mill"),
      MyCityModel(
          imgUrl:
          "https://dandg.azureedge.net/cache/2/2/5/7/f/a/2257fa9015c73278d8fa06c2ce23f2df6d5531c4.jpg",
          cityName: 'London',
          country: 'Britain',
          populationLabel: "Population 25 mill"),
      MyCityModel(
          imgUrl:
          "https://assets.simpleviewinc.com/simpleview/image/upload/c_fill,h_463,q_50,w_800/v1/clients/vancouverbc/The-City_121bc6b2-ca7f-45be-a877-7e166af5502d.jpg",
          cityName: 'Vancouver',
          country: 'Canada',
          populationLabel: "Population 24 mill"),
      MyCityModel(
          imgUrl:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRKOy6uNbpFHw7zWr8FhQIrKWxsuo65lNTi7A&usqp=CAU",
          cityName: 'New York',
          country: 'USA',
          populationLabel: "Population 14 mill"),
      MyCityModel(
          imgUrl:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQTz7z2Ayon7CstLjIlVobhZDzboCrQ30M-MQ&usqp=CAU",
          cityName: 'Sofia',
          country: 'Bulgaria',
          populationLabel: "Population 8 mill")
    ];
  }
}

class MyCityModel {
  String imgUrl;
  String cityName;
  String country;
  String populationLabel;

  MyCityModel({this.imgUrl, this.cityName, this.country, this.populationLabel});
}
