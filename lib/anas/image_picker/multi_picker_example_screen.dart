import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:lottie/lottie.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:platform_action_sheet/platform_action_sheet.dart';
// import 'package:sizer/sizer.dart';

class MultiImagePickerExampleScreen extends StatefulWidget {
  const MultiImagePickerExampleScreen({Key key}) : super(key: key);

  @override
  State<MultiImagePickerExampleScreen> createState() =>
      _MiltiImagePickerExampleScreenState();
}

class _MiltiImagePickerExampleScreenState
    extends State<MultiImagePickerExampleScreen> {
  List<ImageData> _pickedImages = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
            width: MediaQuery.of(context).size.width*0.4,
            height: MediaQuery.of(context).size.height*0.3,
            child: InkWell(
              child: Lottie.asset('assets/plus_icon.json'),
              onTap: () {
                PlatformActionSheet().displaySheet(context: context, actions: [
                  ActionSheetAction(
                      text: 'Camera',
                      onPressed: () {
                        _checkPermission();
                      }),
                  ActionSheetAction(
                      text: 'Gallery',
                      onPressed: () {
                        _checkPermission();
                      }),
                  ActionSheetAction(
                      text: 'Cancel',
                      onPressed: () {
                        Navigator.of(context, rootNavigator: Platform.isIOS)
                            .pop();
                      },
                      isCancel: true),
                ]);
              },
            ),
          ),
          _pickedImages != null && _pickedImages.isNotEmpty
              ? SizedBox(
                  height: MediaQuery.of(context).size.height*0.2,
                  child: ListView.builder(
                    itemBuilder: (context, index) {
                      return Container(
                        width: MediaQuery.of(context).size.width * 0.33,
                        padding: EdgeInsets.all(8.0),
                        child: ClipRRect(
                          borderRadius: BorderRadius.all(Radius.circular(100)),
                          child: AssetThumb(
                            asset: _pickedImages[index].asset,
                            width: 80,
                            height: 80,
                          ),
                        ),
                      );
                    },
                    itemCount: _pickedImages.length,
                    scrollDirection: Axis.horizontal,
                  ),
                )
              : Container(),
        ],
      )),
    );
  }

  Future<void> loadAssets() async {
    List<Asset> resultList = <Asset>[];
    List<Asset> images = [];

    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 5,
        enableCamera: true,
        selectedAssets: images,
        cupertinoOptions: CupertinoOptions(takePhotoIcon: "chat"),
        materialOptions: MaterialOptions(
          actionBarColor: "#abcdef",
          actionBarTitle: "Example App",
          allViewTitle: "Sameer",
          useDetailsView: false,
          selectCircleStrokeColor: "#000000",
        ),
      );
    } on Exception catch (e) {
      print(e); // show dialog
    }
    Navigator.of(context).pop();
    for (var asset in resultList) {
      final data = await asset.getThumbByteData(
        (asset.originalWidth * 0.50).toInt(),
        (asset.originalHeight * 0.50).toInt(),
        quality: 100,
      );
      Uint8List _byteArray =
          data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
      final imageBytes = await FlutterImageCompress.compressWithList(_byteArray,
          quality: 80, autoCorrectionAngle: true);

      final _base64 = base64Encode(_byteArray);
      if (mounted) {
        setState(() {
          _pickedImages.add(
              ImageData(asset: asset, base64: _base64, fileName: asset.name));
        });
      }
      // if (mounted) {
      //   _images.add(ImageObject(
      //     asset: asset,
      //     base64: _base64,
      //     fileName: asset.name,
      //   ));
      // }
    }
  }

  void _checkPermission() async {
    PermissionStatus status = await Permission.camera.status;
    if (status.isGranted) {
      loadAssets();
    } else {
      if (await (Permission.camera.request()).isGranted) {
        loadAssets();
      } else {
        print('Camera can not open'); // show dialog
      }
    }
  }
}

class ImageData {
  String base64;
  Asset asset;
  String fileName;

  ImageData({this.base64, this.asset, this.fileName});
}
