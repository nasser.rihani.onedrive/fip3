import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:lottie/lottie.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:platform_action_sheet/platform_action_sheet.dart';

class ImagePickerScreen extends StatefulWidget {
  const ImagePickerScreen({Key key}) : super(key: key);

  @override
  State<ImagePickerScreen> createState() => _ImagePickerScreenState();
}

class _ImagePickerScreenState extends State<ImagePickerScreen> {
  List<ImageObject> _images = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            _getImage(),
          ],
        ),
      ),
    );
  }

  Widget _getImage() {
    return GestureDetector(
      onTap: () {
        PlatformActionSheet().displaySheet(
          context: context,
          actions: [
            ActionSheetAction(
              text: 'Camera',
              onPressed: () {
                Navigator.of(context,
                        rootNavigator: Platform.isIOS ? true : false)
                    .pop();
                getSinglePermissionStatus(() {
                  _loadAssets();
                });
              },
            ),
            ActionSheetAction(
              text: 'Gallery',
              onPressed: () {
                Navigator.of(context,
                        rootNavigator: Platform.isIOS ? true : false)
                    .pop();
                getSinglePermissionStatus(() {
                  _loadAssets();
                });
              },
            ),
            ActionSheetAction(
                isCancel: true,
                text: 'Cancel',
                onPressed: () {
                  Navigator.of(context,
                          rootNavigator: Platform.isIOS ? true : false)
                      .pop();
                }),
          ],
        );
      },
      child: _images != null && _images.isNotEmpty
          ? _getImagesView()
          : SizedBox(
              height: MediaQuery.of(context).size.height * 0.2,
              child: Lottie.asset('assets/plus_icon_yellow.json'),
            ),
    );
  }

  getSinglePermissionStatus(VoidCallback onPress) async {
    var status = await Permission.camera.status;
    if (status.isDenied) {
      var status = await (Permission.camera.request());
      if (status.isGranted) {
        onPress.call();
      } else {
        getSinglePermissionStatus(onPress);
      }
    } else {
      onPress.call();
    }
  }

  Future<void> _loadAssets() async {
    FocusScope.of(context).unfocus();
    List<Asset> resultList = [];
    List<Asset> selectedAssets = [];

    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 10,
        enableCamera: true,
        selectedAssets: selectedAssets,
        cupertinoOptions: CupertinoOptions(takePhotoIcon: "chat"),
        materialOptions: MaterialOptions(
          actionBarColor: "#1b7249",
          actionBarTitle: 'Fip2',
          allViewTitle: 'All',
          useDetailsView: false,
          selectCircleStrokeColor: "#000000",
        ),
      );
    } on PlatformException catch (e) {}

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    // _images = [];

    for (var asset in resultList) {
      final data = await asset.getThumbByteData(
        (asset.originalWidth * 0.25).toInt(),
        (asset.originalHeight * 0.25).toInt(),
        quality: 60,
      );
      Uint8List _byteArray =
          data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
      final imageBytes = await FlutterImageCompress.compressWithList(_byteArray,
          quality: 100, autoCorrectionAngle: true);

      final _base64 = base64Encode(imageBytes);
      if (mounted) {
        _images.add(ImageObject(
          asset: asset,
          base64: _base64,
          fileName: asset.name,
        ));
      }
    }

    if (mounted) setState(() {});
  }

  Widget _getImagesView() {
    return Container(
      height: 100,
      width: MediaQuery.of(context).size.width,
      child: ListView.builder(
        physics: AlwaysScrollableScrollPhysics(),
        itemBuilder: (context, index) {
          return Container(
            width: MediaQuery.of(context).size.width * 0.33,
            padding: EdgeInsets.all(8.0),
            child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(12)),
                child: AssetThumb(
                  width: 100,
                  height: 100,
                  asset: _images[index].asset,
                )),
            // child: ClipRRect(
            //   borderRadius: BorderRadius.all(Radius.circular(10)),
            //   child: AssetThumb(
            //     asset: _images[index].asset,
            //     width: 100,
            //     height: 100,
            //   ),
            // ),
          );
        },
        shrinkWrap: true,
        itemCount: _images.length,
        scrollDirection: Axis.horizontal,
      ),
    );
  }
}

class ImageObject {
  final File file;
  final Asset asset;
  final String base64;
  final String fileName;
  String imagePath;

  ImageObject({
    this.file,
    this.asset,
    this.base64,
    this.fileName,
  });
}
