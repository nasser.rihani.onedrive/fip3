import 'package:flutter/material.dart';

class ExpandedSample extends StatefulWidget {
  const ExpandedSample({Key key}) : super(key: key);

  @override
  State<ExpandedSample> createState() => _ExpandedSampleState();
}

class _ExpandedSampleState extends State<ExpandedSample> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: sample(),
    );
  }

  Column _columnSample() {
    return Column(children: [
      Expanded(
          child: Container(
            color: Colors.red,
            height: 20,
          ),
          flex: 2),
      // 1/4
      Expanded(
          child: Container(
            color: Colors.yellow,
            height: 40,
          ),
          flex: 3),
      // 1/4
      Expanded(
        child: Container(
          color: Colors.blue,
          height: 40,
        ),
        flex: 5,
      ),
      // 2/4
    ]);
  }

  Row _rowSample() {
    return Row(children: [
      Expanded(
        child: Container(
          color: Colors.red,
          width: 20,
        ),
        flex: 2,
      ),
      // 1/4
      Expanded(
        child: Container(
          color: Colors.yellow,
          width: 40,
        ),
        flex: 2,
      ),
      // 1/4
      Expanded(
        child: Container(
          color: Colors.blue,
          width: 40,
        ),
        flex: 4,
      ),
      // 2/4
    ]);
  }

  Widget _columnSample2() {
    return Center(
      child: Column(
        children: [
          Expanded(
              child: Container(
                height: 20,
                child: const Icon(Icons.add),
                color: Colors.red,
              ),
              flex: 1),
          // 1/4
          Expanded(
            child: Container(
                height: 40, child: const Icon(Icons.add), color: Colors.green),
            flex: 1,
          ),
          // 1/4
          Expanded(
            child: Container(
                height: 40, child: const Icon(Icons.add), color: Colors.black),
            flex: 2,
          ),
          // 2/4
        ],
        crossAxisAlignment: CrossAxisAlignment.center,
      ),
    );
  }

  Widget spacer_sample() {
    return Center(
        child: Column(
      children:const [
        Text('Ahmad'),
        Text('Ahmad'),
        Spacer(),
        Text('anas'),
        Text('anas'),
        Text('anas'),
        Text('anas'),
        Text('anas'),
        Text('anass'),
      ],
      crossAxisAlignment: CrossAxisAlignment.center,
    ));
  }

  Widget sample() {
    print(MediaQuery.of(context).size.width);
    print(MediaQuery.of(context).size.height);
    return Center(
      child: Container(
        // color: Colors.red,
        width: MediaQuery.of(context).size.width / 2,
        height: MediaQuery.of(context).size.height / 2,
        alignment: Alignment.center,
        child: const Text(
          'anas',
          style: TextStyle(color: Colors.white),
        ),
        padding: EdgeInsets.zero,
        margin:
            EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.20),
        decoration: const BoxDecoration(
            // color: Colors.red,
            borderRadius: BorderRadius.all(Radius.circular(10)),
            gradient: LinearGradient(
                colors: [Colors.red, Colors.pink, Colors.orange, Colors.black],
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                stops: [3, 0.2, 0.5, 5]),
            boxShadow: [BoxShadow(color: Colors.black, blurRadius: 20)]),
      ),
    );
  }
}
