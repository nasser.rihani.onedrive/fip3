import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:regexpattern/regexpattern.dart';

class TextFormField2 extends StatefulWidget {
  const TextFormField2({Key key}) : super(key: key);

  @override
  State<TextFormField2> createState() => _TextFormFieldState();
}

class _TextFormFieldState extends State<TextFormField2> {
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  bool _isObscureText = false;
  static const List<String> _kOptions = <String>[
    'Hamzah',
    'Anas',
    'Omar',
    'Motasem',
    'Moahmmed',
    'Suhaib',
    'Ahmad',
  ];
  final _key=GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: Container(
        margin: EdgeInsets.symmetric(horizontal: 20),
        child: Form(
          key: _key,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              TextFormField(
                decoration: InputDecoration(
                    labelText: 'email',
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(14))),
                controller: _emailController,
                // obscureText: _isObscureText,
                keyboardType: TextInputType.text,
                autovalidateMode: AutovalidateMode.always,
                validator: (value){
                  if(value.isEmpty) {
                    return 'this field is required';
                  } else if (!value.isEmail()){
                    return 'this is not a valid email';
                  }
                  else {
                    return null;
                  }
                },
              ),
              SizedBox(
                height: 8,
              ),
              TextFormField(
                decoration: InputDecoration(
                    labelText: 'Password',
                    suffixIcon: IconButton(
                        icon: Icon(_isObscureText
                            ? Icons.remove_red_eye
                            : Icons.visibility_off),
                        onPressed: () {
                          setState(() {
                            _isObscureText = !_isObscureText;
                          });
                        }),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(14))),
                controller: _passwordController,
                // focusNode: _passwordFocus,
                obscureText: _isObscureText,
                autovalidateMode: AutovalidateMode.always,
                inputFormatters: [
                  FilteringTextInputFormatter.allow(RegExp(r'^[a-zA-Z0-9]+$'))
                ],
                validator: (value){
                  if(value.isEmpty){
                    return 'this field is required';
                  }
                  // else if (!value.isPasswordHard()){
                  //   return 'this is an easy password';
                  // }
                  else {
                    return null;
                  }
                },
              ),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Autocomplete<String>(
                  optionsBuilder: (TextEditingValue textEditingValue) {
                    if (textEditingValue.text.isEmpty) {
                      return const Iterable<String>.empty();
                    }
                    return _kOptions.where((String option) {
                      return option.contains(
                          textEditingValue.text.toLowerCase());
                    });
                  },
                  onSelected: (String selection) {
                    debugPrint('You just selected $selection');
                  },
                ),
              ),
              SizedBox(child: Divider(color: Colors.grey,thickness: 1.5,height: 5),width: 100,),
              ElevatedButton(
                  onPressed: () {
                   _key.currentState.validate();
                  },
                  child: const Text('Submit'))
            ],
          ),
        ),
      )),
    );
  }
}
