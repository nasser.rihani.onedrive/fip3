import 'package:flutter/material.dart';

class TaskUi extends StatefulWidget {
  const TaskUi({Key key}) : super(key: key);

  @override
  State<TaskUi> createState() => _TaskUiState();
}

class _TaskUiState extends State<TaskUi> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Container(
              height: MediaQuery.of(context).size.height * 0.05,
            ),
            Container(
              height: MediaQuery.of(context).size.height * 0.15,
              width: MediaQuery.of(context).size.width * 0.5,
              color: Colors.red,
              child: Column(
                children: [
                  Icon(Icons.minimize),
                  Icon(Icons.add),
                  Icon(Icons.add),
                  Expanded(child: Icon(Icons.minimize)),
                ],
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height * 0.1,
            ),
            Container(
              height: MediaQuery.of(context).size.height * 0.17,
              width: MediaQuery.of(context).size.width * 0.6,
              color: Colors.blue,
              child: Column(
                children:const [
                  Icon(Icons.minimize),
                  Expanded(child: Icon(Icons.add)),
                  Icon(Icons.add),
                  Expanded(child: Icon(Icons.minimize)),
                ],
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height * 0.11,
            ),
            Container(
              height: MediaQuery.of(context).size.height * 0.2,
              width: MediaQuery.of(context).size.width * 0.7,
              color: Colors.blue,
              child: Column(
                children: [
                  Expanded(child: Icon(Icons.add)),
                  Icon(Icons.minimize),
                  Icon(Icons.add),
                  Expanded(child: Icon(Icons.minimize)),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
