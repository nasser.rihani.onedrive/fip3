import 'package:flutter/material.dart';

class MixWidgets3 extends StatefulWidget {
  const MixWidgets3({Key key}) : super(key: key);

  @override
  State<MixWidgets3> createState() => _MixWidgets3State();
}

class _MixWidgets3State extends State<MixWidgets3> {
  bool isChecked = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Row(
                children: [
                  Text('Are you above 22 years old ? '),
                  Checkbox(
                    value: isChecked,
                    onChanged: (value) {
                      setState(() {
                        isChecked = !isChecked;
                      });
                    },
                    activeColor: Colors.blue,
                    focusColor: Colors.red,
                    hoverColor: Colors.yellow,
                    checkColor: Colors.brown,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20)),
                  )
                ],
              ),
              ListTile(
                leading: Text('Are you above 22 years old ? '),
                trailing: Checkbox(
                  value: isChecked,
                  onChanged: (value) {
                    setState(() {
                      isChecked = !isChecked;
                    });
                  },
                  activeColor: Colors.blue,
                  focusColor: Colors.red,
                  hoverColor: Colors.yellow,
                  checkColor: Colors.brown,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20)),
                ),
                enableFeedback: true,
              ),
            ]),
      ),
    );
  }
}
//todo MaterialColor for primary swatch
// ListView.builder, GridView.builder,  future builder, stream builder
